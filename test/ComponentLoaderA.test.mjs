import {describe, expect, test} from '@jest/globals';
import {format} from 'util';
import {fileURLToPath} from 'url';
import {dirname} from 'path';
import mock from 'jest-mock';

describe(
    'ComponentLoader',
    () => {
        describe('load', () => {
            test('load method is able to load a component from a file', () => new Promise(
                resolve => {
                    Promise.all(
                        [import('../src/ComponentLoader.mjs'), import('./fixture/component/A1/component.fixture.mjs')]
                    ).then(modules => {
                        const file = format(
                            '%s/./fixture/component/A1/component.fixture.mjs',
                            dirname(fileURLToPath(import.meta.url))
                        );

                        modules[0].default.load(file).then(obj => {
                            expect(obj).toBe(modules[1]);
                            resolve();
                        });
                    });
                })
            );
        });
    }
);
