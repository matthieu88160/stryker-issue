import {existsSync} from 'fs';
import {format} from 'util';

/**
 * Component loader take care of component loading
 */
export default class ComponentLoader
{
    /**
     * @param {string} file The file to load
     *
     * @returns {Promise<{}>} A promise corresponding to the module import
     * @throws {Error}
     */
    static load(file) {
        if(existsSync(file)) {
            return import(file);
        }

        throw new Error(
            format('Component file "%s" does not exist. Cannot load module', file)
        );
    }
}
